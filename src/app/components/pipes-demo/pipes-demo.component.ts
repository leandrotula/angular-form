import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-demo',
  templateUrl: './pipes-demo.component.html',
  styleUrls: ['./pipes-demo.component.css']
})
export class PipesDemoComponent implements OnInit {

  nombre: string = 'leandro';
  nombreaux: string = 'leandro';
  upperCaseName: string = 'LEANDRO';
  arrayData: number[] = [1, 2, 3, 4, 5, 6, 7, 8];
  PI: any = Math.PI;
  auxDecimals: any = 0.2345555;
  salario: any = 12344.32223232;

  constructor() {


  }

  ngOnInit() {
  }

}
