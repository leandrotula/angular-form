import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-contact-data-form',
  templateUrl: './contact-data-form.component.html',
  styleUrls: ['./contact-data-form.component.css']
})
export class ContactDataFormComponent implements OnInit {

  contactDataForm: FormGroup;

  usuario: Object = {
    nombrecompleto : {
      nombre: '',
      apellido: ''
    },
    correo: '',
    user: ''
  };

  constructor() {

    this.contactDataForm = new FormGroup({

      'nombrecompleto': new FormGroup({
        'nombre' : new FormControl('', Validators.required),
        'apellido' : new FormControl('', Validators.required),
      }),
      'correo' : new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'password1': new FormControl(''),
      'password2': new FormControl(''),
      'usuario' : new FormControl('', Validators.required, this.doesUserExists),
      'pasatiempos' : new FormArray([
        new FormControl('', Validators.required)
      ])
    });

    this.contactDataForm.controls['password1'].setValidators(Validators.required);
    this.contactDataForm.controls['password2'].setValidators([Validators.required,
      this.passwordMatches.bind(this.contactDataForm)]);

    //this.contactDataForm.setValue(this.usuario);
  }

  ngOnInit() {
  }

  submitForm(): void {
    console.log('submit form', this.contactDataForm);
    console.log('submit values', this.contactDataForm.value);

    //this.contactDataForm.reset();

    this.contactDataForm.reset({
      nombrecompleto: {
        nombre: '',
        apellido: ''
      },
      correo: '',
      'usuario' : '',
      'pasatiempos' : []
    });
  }

  addHobbies() : void {
    (<FormArray>this.contactDataForm.controls['pasatiempos']).push(
      new FormControl('', Validators.required)
    );
  }

  passwordMatches(formControl : FormControl): {[ s : string ] : boolean} {

    let contactForm: any = this;

    if (formControl.value !== contactForm.controls['password1'].value) {

      return {
        passwordDoesNotMatches: true
      }
    }

    return null;
  }

  doesUserExists(formControl : FormControl) : Promise<any> | Observable<any> {

    let promise = new Promise(

      (resolve, rejected) => {

        setTimeout(() => {

          if (formControl.value === 'emi') {

            resolve ( {
              existsUser : true
            })

          } else {

            resolve (null)
          }

        }, 3000);

      }

    );

    return promise;


  }

}
