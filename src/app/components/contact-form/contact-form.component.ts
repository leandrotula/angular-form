import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  contactInfo:Object = {

    name: '',
    lastname: '',
    emailAddress: '',
    contactMedia: '',
    showContactOptions: false,
    feedback: '',
    phone: '',
    codeArea: ''
  };

  contactMedia = [{
    codigo: 'phone',
    value:'Phone'
  },
    {
      codigo: 'email',
      value:'Email'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  submitForm(contactForm: NgForm): void {
    console.log('Submitting form .....');

    console.log(contactForm);

    console.log('contact info .....');

    console.log(this.contactInfo);

  }

}
