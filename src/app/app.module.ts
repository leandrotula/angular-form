import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ContactDataFormComponent } from './components/contact-data-form/contact-data-form.component';
import {APP_ROUTING} from "../app.routes";
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PipesDemoComponent } from './components/pipes-demo/pipes-demo.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactFormComponent,
    ContactDataFormComponent,
    HomeComponent,
    NavbarComponent,
    PipesDemoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
