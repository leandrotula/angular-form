import {RouterModule, Routes} from '@angular/router';
import {ContactFormComponent} from './app/components/contact-form/contact-form.component';
import {HomeComponent} from "./app/components/home/home.component";
import {ContactDataFormComponent} from "./app/components/contact-data-form/contact-data-form.component";
import {PipesDemoComponent} from "./app/components/pipes-demo/pipes-demo.component";

const APPROUTES: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'contactForm', component: ContactFormComponent},
  { path: 'dataForm', component: ContactDataFormComponent},
  { path: 'demoPipes', component: PipesDemoComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full'
  }
];

export const APP_ROUTING = RouterModule.forRoot(APPROUTES);
